package com.example.myapplication1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.myapplication1.Utils.EmployeeDataUtils;
import com.example.myapplication1.models.Employee;

public class MainActivity4 extends AppCompatActivity {
    private Spinner spinnerEmployee;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);
        spinnerEmployee = findViewById(R.id.spinner_employee);
        Employee[] employees = EmployeeDataUtils.getEmployee();
        // (@resource) android.R.layout.simple_spinner_item:
        //   The resource ID for a layout file containing a TextView to use when instantiating views.
        //    (Layout for one ROW of Spinner)
        ArrayAdapter<Employee> adapter = new ArrayAdapter<Employee>(this, R.layout.support_simple_spinner_dropdown_item,employees);
        // Layout for All ROWs of Spinner.  (Optional for ArrayAdapter).
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        // When user select a List-Item.
        this.spinnerEmployee.setAdapter(adapter);
        this.spinnerEmployee.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                onItemSelectedHandler(parent,view,position,id);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
    private void onItemSelectedHandler(AdapterView<?> adapterView, View view, int position, long id) {
        Adapter adapter = adapterView.getAdapter();
        Employee employee = (Employee) adapter.getItem(position);
        Toast.makeText(this, "Selected Employee: " + employee.getFullName() ,Toast.LENGTH_SHORT).show();
    }
}