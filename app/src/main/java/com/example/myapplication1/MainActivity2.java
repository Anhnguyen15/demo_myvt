package com.example.myapplication1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class MainActivity2 extends AppCompatActivity {

    EditText editText1, editText2;
    Button btnRandom;
    TextView textViewResult;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Log.e("TAG", "onCreate");
        mappingItem();
        btnRandom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String num1 = editText1.getText().toString().trim();
                String num2 = editText2.getText().toString().trim();
                if (num1.isEmpty() || num2.isEmpty()) {
                    Toast.makeText(MainActivity2.this,"Nhap du so!", Toast.LENGTH_SHORT).show();
                } else {
                    int min = Integer.parseInt(num1);
                    int max = Integer.parseInt(num2);
                    Random random = new Random();
                    int number = random.nextInt(max - min + 1) + min;
                    textViewResult.setText(String.valueOf(number));
                }

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.e("TAG", "onStart");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e("TAG", "onPause");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("TAG", "onResume");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e("TAG", "onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e("TAG", "onDestroy");
    }

    void mappingItem() {
        editText1 = findViewById(R.id.edt1);
        editText2 = findViewById(R.id.edt2);
        btnRandom = findViewById(R.id.btnRandom);
        textViewResult = findViewById(R.id.txtResult);
    }
}