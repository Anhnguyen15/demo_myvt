package com.example.myapplication1.Utils;

import com.example.myapplication1.models.Employee;

public class EmployeeDataUtils {
    public static Employee[] getEmployee() {
        Employee employee1 = new Employee("ha","le","1",10000000);
        Employee employee2 = new Employee("nam","le","2",20000000);
        Employee employee3 = new Employee("lien","nguyen","3",30000000);
        return new Employee[]{employee1,employee2,employee3};
    }
}
